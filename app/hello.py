from flask import Flask
from flask_cors import cross_origin
app = Flask(__name__)

@app.route('/', methods=["POST"])
@cross_origin(supports_credentials=True)
def hello():
    return "Hello gitlab third try message!"

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug= True, port = 6000)
