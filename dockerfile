from alpine:latest
RUN apk add --no-cache python3-dev &&\
    pip3 install --upgrade pip 

WORKDIR /app

COPY . /app
EXPOSE 5000

RUN pip install --trusted-host pypi.python.org -r requirements.txt
WORKDIR /app/app/
ENTRYPOINT ["python3"]
CMD ["hello.py"]